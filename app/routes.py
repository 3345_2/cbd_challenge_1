from app import app


@app.route("/", methods=["GET"])
@app.route("/index", methods=["GET"])
@app.route("/homepage", methods=["GET"])
@app.route("/dashboard", methods=["GET"])
def index():
    return “Welcome Ederlinda! You are authenticated to use the API.”